          <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
            <li><a href="<?php echo '<?php echo $this->webroot; ?>'; ?>"<i class="fa fa-home"></i> Home</a></li>
            <li class="active"><?php echo "<?php echo __('{$singularHumanName}'); ?>"; ?> List</li>
          </ul>
          <div class="m-b-md">
            <h3 class="m-b-none"><?php echo "<?php echo __('{$singularHumanName}'); ?>"; ?> List</h3>
          </div>

              <section class="panel panel-default">
                <div class="row wrapper">
                  <div class="col-sm-5 m-b-xs">
                      <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-default" title="Refresh" onclick ='location.reload();'><i class="fa fa-refresh"></i></button>
                      </div>
                       <?php 
                        echo "\t\t\t<?php echo \$this->Html->link(' Create', array('action' => 'add'), array('class' => 'btn btn-success fa fa-plus', 'data-toggle'=>'ajaxModal', 'escape' => false)); ?>\n";
                      ?> 
                  </div>
                  <div class="col-sm-4 m-b-xs">
                    &nbsp;
                  </div>
                  <div class="col-sm-3">
                      <?php echo "<?php echo \$this->Form->create('{$modelClass}', array('class' => 'bs-example form-horizontal', 'inputDefaults' => array('label' => false, 'div' => false))); ?>\n"; ?>
                      <div class="input-group">
                      <?php
                      echo "\t<?php\n";
                      echo "\t\techo \$this->Form->input('queryString', array('class' => 'input-sm form-control', 'placeholder' => 'Search')); ?>\n";
                      echo "<span class='input-group-btn'>\n";
                      echo "<?php echo \$this->Form->button('Go!', array('class' => 'btn btn-sm btn-default bg-success'));?>\n";
                      echo "</span>\n\t\t</div>";
                      echo "<?php echo \$this->Form->end();?>\n";
                      ?>
                    </div>
                  </div>
                </div>
                <div class="table-responsive">
                  <table class="table table-striped b-t b-light">
                    <thead>
                      <tr>
                        
                        <th class="col-md-2 text-center sorting"><?php echo "<?php echo __('Actions'); ?>"; ?></th>
                      <?php foreach ($fields as $field): ?>
                      <?php if (!in_array($field, array('created', 'modified','id'))) : ?>
            						<th>
            							 <?php echo "<?php echo \$this->Paginator->sort('{$field}'); ?>"; ?>
            						</th>
                        <?php endif; ?>
            					<?php endforeach; ?>
                        <th width="30"></th>
                      </tr>
                    </thead>
                    <tbody>
                      
                      <?php
						echo "<?php foreach (\${$pluralVar} as \${$singularVar}): ?>\n";
						echo "\t<tr>\n";
            echo "\t\t<td class=\"actions\">\n";
              echo "\t\t\t<?php echo \$this->Html->link(\$this->Form->button('<i class=\"fa fa-desktop\"></i>', array('class'=>'btn btn-xs btn-success', 'style'=>'color:#000; width:25px; height:25px;','data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>'View Details')), array('action' => 'view', \${$singularVar}['{$modelClass}']['{$primaryKey}']), array('escape' => false)); ?>\n";
              echo "\t\t\t<?php echo \$this->Html->link(\$this->Form->button('<i class=\"fa fa-pencil\"></i>', array('class'=>'btn btn-xs btn-warning', 'style'=>'color:#000; width:25px; height:25px;','data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>'Modify Details')), array('action' => 'edit', \${$singularVar}['{$modelClass}']['{$primaryKey}']), array('data-toggle'=>'ajaxModal', 'escape' => false)); ?>\n";
              
              echo "\t\t</td>\n";
							foreach ($fields as $field) {
								$isKey = false;
								if (!empty($associations['belongsTo'])) {
									foreach ($associations['belongsTo'] as $alias => $details) {
										if ($field === $details['foreignKey']) {
											$isKey = true;
											echo "\t\t<td>\n\t\t\t<?php echo \$this->Html->link(\${$singularVar}['{$alias}']['{$details['displayField']}'], array('controller' => '{$details['controller']}', 'action' => 'view', \${$singularVar}['{$alias}']['{$details['primaryKey']}'])); ?>\n\t\t</td>\n";
											break;
										}
									}
								}
								if ($isKey !== true && !in_array($field, array('created', 'modified', 'id', 'active'))) {
									echo "\t\t<td><?php echo h(\${$singularVar}['{$modelClass}']['{$field}']); ?>&nbsp;</td>\n";
								} elseif ($isKey !== true && in_array($field, array('active'))) {
                  echo "\t\t\t\t\t<?php if (\${$singularVar}['{$modelClass}']['{$field}'] == 1) { ?>\n";
                  echo "\t\t\t\t\t\t<td><i class='fa fa-check bg-success' style='color:#fff; width:25px; height:25px;padding:5px'></i> &nbsp;</td>\n";
                  echo "\t\t\t\t\t<?php } else { ?>\n";
                  echo "\t\t\t\t\t\t<td><i class='fa fa-times bg-danger' style='color:#fff; width:25px; height:25px;padding:5px'></i> &nbsp;</td>\n";
                  echo "\t\t\t\t\t<?php } ?>\n";

                }
							}
              
						echo "\t</tr>\n";

						echo "<?php endforeach; ?>\n";
						?>
						
                    </tbody>
                  </table>
                </div>
                <footer class="panel-footer">
                  <div class="row">
                    <div class="col-sm-4 text-left hidden-xs">
                      &nbsp; 

                    </div>
                    <div class="col-sm-4 text-center">
                      <small class="text-muted inline m-t-sm m-b-sm">
                     	<?php echo "<?php
									echo \$this->Paginator->counter(array(
										'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
									));
									?>"; ?>
                      </small>
                    </div>
                    <div class="col-sm-4 text-right text-center-xs">                
                      <ul class="pagination pagination-sm m-t-none m-b-none">
                        
                        <?php
							echo "<?php\n";
              echo "\t\techo '<li>'.\$this->Paginator->first(__('first'), array(), null, array('class' => 'prev disabled')).'</li>';\n";
							echo "\t\techo '<li>'.\$this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled')).'</li>';\n";
							echo "\t\techo '<li>'.\$this->Paginator->numbers(array('separator' => '</li>
                        <li>')).'</li>';\n";
              echo "\t\techo '<li>'.\$this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled')).'</li>';\n";
							echo "\t\techo '<li>'.\$this->Paginator->last(__('last'), array(), null, array('class' => 'next disabled')).'</li>';\n";
							echo "\t?>\n";
						?>
                      </ul>
                    </div>
                  </div>
                </footer>
              </section>

