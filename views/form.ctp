<?php
$printCSS = false;

foreach ($fields as $field) {
	if (in_array($field, array('start_date', 'end_date', 'completed_on', 'last_enrollment'))) { 
		$printCSS = true;
	}
}

if ($printCSS) { 
?>
<style type="text/css">
	.datepicker{z-index:9999 !important}";
</style>

<?php  } ?>


<div class="modal-dialog">
  <div class="modal-content">
  	<?php echo "<?php echo \$this->Form->create('{$modelClass}', array('type' => 'file', 'class' => 'bs-example form-horizontal')); ?>\n"; ?>
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title"><?php printf("<?php echo __('%s %s'); ?>", Inflector::humanize($action), $singularHumanName); ?></h4>
    </div>
    <div class="modal-body">
			<?php
					echo "\t<?php\n";
					foreach ($fields as $field) {
						if (strpos($action, 'add') !== false && $field === $primaryKey) {
							continue;
						} elseif (!in_array($field, array('created', 'modified', 'updated', 'files','start_date', 'end_date', 'completed_on', 'last_enrollment'))) {
							echo "\t\techo \$this->Form->input('{$field}', array('class' => 'form-control'));\n";
						} elseif (in_array($field, array('files'))) {
							echo "\t\techo \$this->Form->input('{$field}', array('type' => 'file', 'class' => 'col-lg-2 control-label form-control'));\n";
						} elseif (in_array($field, array('start_date', 'end_date', 'completed_on', 'last_enrollment'))) {
							echo "\t\techo \$this->Form->input('{$field}', array('class' => 'datepicker-input control-label form-control', 'value' => date(\"d-m-Y\"), 'data-date-format' => 'dd-mm-yyyy', 'type' => 'text', 'style' => 'z-index: 100000!important;'));\n";
						}
					}
					if (!empty($associations['hasAndBelongsToMany'])) {
						foreach ($associations['hasAndBelongsToMany'] as $assocName => $assocData) {
							echo "\t\techo \$this->Form->input('{$assocName}', array('class' => 'form-control'));\n";
						}
					}
					echo "\t?>\n";
			?>
	</div>
    <div class="modal-footer">
    	<?php   echo "<?php echo \$this->Layout->sessionFlash(); ?>\n"; ?>
    	<a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
    	<?php   echo "<?php echo \$this->Form->button('Submit', array('class' => 'btn btn-primary'));?>\n"; ?>
    </div>
    	<?php   echo "<?php echo \$this->Form->end();?>\n"; ?>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->


<?php
foreach ($fields as $field) {
	if (in_array($field, array('start_date', 'end_date', 'completed_on', 'last_enrollment'))) { 
		$printCSS = true;
	}
}

if ($printCSS) { 
?>
<script type="text/javascript">
<?php } ?>
<?php
		foreach ($fields as $field) {
			if (in_array($field, array('start_date', 'end_date', 'completed_on', 'last_enrollment'))) { 
				$idVar = ucwords($singularHumanName).ucwords(str_replace('_', ' ', $field));
?>
				$('#<?php echo str_replace(' ', '', $idVar);?>').datepicker({
				     dateFormat: 'dd-mm-yy',
				     minDate: '+5d',
				     changeMonth: true,
				     changeYear: true
				 });
<?php
			}
		}
		
?>
<?php
foreach ($fields as $field) {
	if (in_array($field, array('start_date', 'end_date', 'completed_on', 'last_enrollment'))) { 
		$printCSS = true;
	}
}

if ($printCSS) { 
?>
</script>
<?php } ?>
