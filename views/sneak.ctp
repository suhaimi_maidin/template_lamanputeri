          <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
            <li><a href="<?php echo '<?php echo $this->webroot; ?>'; ?>"<i class="fa fa-home"></i> Home</a></li>
            <li class="active"><a href="#" onClick="location.reload();">Sneak Preview: <?php echo "<?php echo __('{$singularHumanName}'); ?>"; ?>'s Details </a></li>
          </ul>
          <div class="m-b-md">
            <h3 class="m-b-none">Sneak Preview: <?php echo "<?php echo __('{$singularHumanName}'); ?>"; ?>'s Details </h3>
          </div>
          <!-- start content view page -->
          <section class="vbox">
            <section class="scrollable">
              <section class="hbox stretch">
                <!-- start column 1 -->
                <aside class="aside-lg bg-light lter b-r">
                  <section class="vbox">
                    <section class="scrollable">
                      <div class="wrapper">
                        
                        <div class="portlet">
                          <section class="panel panel-info portlet-item">
                            <header class="panel-heading">
                            Information
                          </header>
                            <?php
                              foreach ($fields as $field) {
                                $isKey = false;
                                if (!empty($associations['belongsTo'])) {
                                  foreach ($associations['belongsTo'] as $alias => $details) {
                                    if ($field === $details['foreignKey']) {
                                      $isKey = true;
                                      echo "\t\t<small class=\"text-uc text-xs text-muted\"><?php echo __('" . Inflector::humanize(Inflector::underscore($alias)) . "'); ?></small>\n";
                                      echo "\t\t<p>\n\t\t\t<?php echo \$this->Html->link(\${$singularVar}['{$alias}']['{$details['displayField']}'], array('controller' => '{$details['controller']}', 'action' => 'view', \${$singularVar}['{$alias}']['{$details['primaryKey']}'])); ?>\n\t\t\t&nbsp;\n\t\t</p>\n";
                                      break;
                                    }
                                  }
                                }
                                if ($isKey !== true) {
                                  echo "\t\t<small class=\"text-uc text-xs text-muted\"><?php echo __('" . Inflector::humanize($field) . "'); ?></small>\n";
                                  echo "\t\t<p>\n\t\t\t<?php echo h(\${$singularVar}['{$modelClass}']['{$field}']); ?>\n\t\t\t&nbsp;\n\t\t</p>\n";
                                }
                              }
                            ?>
                          <div class="line"></div>
                        </section>
                        <section class="panel panel-info portlet-item">
                          <header class="panel-heading">
                            <ul class="nav nav-pills pull-right">
                                      <li>
                                        <a href="#" class="panel-toggle text-muted"><i class="fa fa-caret-down text-active"></i><i class="fa fa-caret-up text"></i></a>
                                      </li>
                                    </ul>
                            Quick links
                          </header>
                          <div class="list-group bg-white">
                           <?php
                              echo "<?php echo \$this->Html->link(__('Edit " . $singularHumanName ."'), array('action' => 'edit', \${$singularVar}['{$modelClass}']['{$primaryKey}']), array('class'=>'list-group-item')); ?> ";
                              echo "<?php echo \$this->Form->postLink(__('Delete " . $singularHumanName . "'), array('action' => 'delete', \${$singularVar}['{$modelClass}']['{$primaryKey}']), array('class'=>'list-group-item'), __('Are you sure you want to delete # %s?', \${$singularVar}['{$modelClass}']['{$primaryKey}'])); ?>  ";
                              echo "<?php echo \$this->Html->link(__('List " . $pluralHumanName . "'), array('action' => 'index'), array('class'=>'list-group-item')); ?> ";
                              echo "<?php echo \$this->Html->link(__('New " . $singularHumanName . "'), array('action' => 'add'), array('class'=>'list-group-item')); ?>  ";
                        ?>
                        </div>
                        </section>
                        <section class="panel panel-info portlet-item">
                          <header class="panel-heading">
                            <ul class="nav nav-pills pull-right">
                                      <li>
                                        <a href="#" class="panel-toggle text-muted"><i class="fa fa-caret-down text-active"></i><i class="fa fa-caret-up text"></i></a>
                                      </li>
                                    </ul>
                            Associated links
                          </header>
                          <div class="list-group bg-white">
                          <?php
                              $done = array();
                              foreach ($associations as $type => $data) {
                                foreach ($data as $alias => $details) {
                                  if ($details['controller'] != $this->name && !in_array($details['controller'], $done)) {
                                    echo "<?php echo \$this->Html->link(__('List " . Inflector::humanize($details['controller']) . "'), array('controller' => '{$details['controller']}', 'action' => 'index'), array('class' => 'list-group-item')); ?> &nbsp;&nbsp; ";
                                    echo "<?php echo \$this->Html->link(__('New " . Inflector::humanize(Inflector::underscore($alias)) . "'), array('controller' => '{$details['controller']}', 'action' => 'add'), array('class' => 'list-group-item')); ?> &nbsp;&nbsp; ";
                                    $done[] = $details['controller'];
                                  }
                                }
                              }
                            ?>
                          </div>
                        </section>
                        </div>
                      </div>
                    </section>
                  </section>
                </aside>
                <!-- end column 1 -->
                
              </section>
            </section>
          </section>
          <!-- end content view page -->