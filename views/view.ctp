          <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
            <li><a href="<?php echo '<?php echo $this->webroot; ?>'; ?>"<i class="fa fa-home"></i> Home</a></li>
            <li class="active"><a href="#" onClick="location.reload();"><?php echo "<?php echo __('{$singularHumanName}'); ?>"; ?>'s Details</a></li>
          </ul>
          <div class="m-b-md">
            <h3 class="m-b-none"><?php echo "<?php echo __('{$singularHumanName}'); ?>"; ?>'s Details</h3>
          </div>
          <!-- start content view page -->
          <section class="vbox">
            <section class="scrollable">
              <section class="hbox stretch">
                <!-- start column 1 -->
                <aside class="aside-lg bg-light lter b-r">
                  <section class="vbox">
                    <section class="scrollable wrapper">
                      <!-- portlet -->
                      <div class="portlet">
                        <section class="panel panel-info portlet-item">
                            <header class="panel-heading">
                            Information
                            </header>
                            <?php
                              foreach ($fields as $field) {
                                if (!in_array($field, array('created', 'modified'))) {
                                  $isKey = false;
                                  if (!empty($associations['belongsTo'])) {
                                    foreach ($associations['belongsTo'] as $alias => $details) {
                                      if ($field === $details['foreignKey']) {
                                        $isKey = true;
                                        echo "\t\t<small class=\"text-uc text-xs text-muted\"><?php echo __('" . Inflector::humanize(Inflector::underscore($alias)) . "'); ?></small>\n";
                                        echo "\t\t<p>\n\t\t\t<?php echo \$this->Html->link(\${$singularVar}['{$alias}']['{$details['displayField']}'], array('controller' => '{$details['controller']}', 'action' => 'view', \${$singularVar}['{$alias}']['{$details['primaryKey']}'])); ?>\n\t\t\t&nbsp;\n\t\t</p>\n";
                                        break;
                                      }
                                    }
                                  }
                                  if ($isKey !== true) {
                                    echo "\t\t<small class=\"text-uc text-xs text-muted\"><?php echo __('" . Inflector::humanize($field) . "'); ?></small>\n";
                                    echo "\t\t<p>\n\t\t\t<?php echo h(\${$singularVar}['{$modelClass}']['{$field}']); ?>\n\t\t\t&nbsp;\n\t\t</p>\n";
                                  }
                                }
                              }
                            ?>
                        </section>
                        <section class="panel panel-info portlet-item">
                          <header class="panel-heading">
                            Quick links
                          </header>
                          <div class="list-group bg-white">
                           <?php
                              echo "<?php echo \$this->Html->link(__('Edit " . $singularHumanName ."'), array('action' => 'edit', \${$singularVar}['{$modelClass}']['{$primaryKey}']), array('class'=>'list-group-item', 'data-toggle'=>'ajaxModal')); ?> ";
                              echo "<?php echo \$this->Form->postLink(__('Delete " . $singularHumanName . "'), array('action' => 'delete', \${$singularVar}['{$modelClass}']['{$primaryKey}']), array('class'=>'list-group-item'), __('Are you sure you want to delete # %s?', \${$singularVar}['{$modelClass}']['{$primaryKey}'])); ?>  ";
                              echo "<?php echo \$this->Html->link(__('List " . $pluralHumanName . "'), array('action' => 'index'), array('class'=>'list-group-item')); ?> ";
                              echo "<?php echo \$this->Html->link(__('New " . $singularHumanName . "'), array('action' => 'add'), array('class'=>'list-group-item', 'data-toggle'=>'ajaxModal', 'escape' => false)); ?>  "; 
                              ?>
                            </div>
                        </section>
                        <section class="panel panel-info portlet-item">
                          <header class="panel-heading">
                            Associated links
                          </header>
                          <div class="list-group bg-white">
                          <?php
                              $done = array();
                              foreach ($associations as $type => $data) {
                                foreach ($data as $alias => $details) {
                                  if ($details['controller'] != $this->name && !in_array($details['controller'], $done)) {
                                    echo "<?php echo \$this->Html->link(__('List " . Inflector::humanize($details['controller']) . "'), array('controller' => '{$details['controller']}', 'action' => 'index'), array('class' => 'list-group-item')); ?> &nbsp;&nbsp; ";
                                    echo "<?php echo \$this->Html->link(__('New " . Inflector::humanize(Inflector::underscore($alias)) . "'), array('controller' => '{$details['controller']}', 'action' => 'add'), array('class' => 'list-group-item')); ?> &nbsp;&nbsp; ";
                                    $done[] = $details['controller'];
                                  }
                                }
                              }
                            ?>
                          </div>
                        </section>
                      </div>
                      <!-- end portlet -->
                    </section>
                  </section>
                </aside>
                <!-- end column 1 -->
                <!-- start column 2 -->
                <?php
                  if (!empty($associations['hasOne']) || !empty($associations['hasMany']) || !empty($associations['hasAndBelongsToMany'])) :
                ?>
                <aside class="bg-white">
                  <section class="vbox">
                    <!-- tab content headers -->
                    <header class="header bg-light bg-gradient" style="height:auto">
                      <div>
                        <ul class="nav nav-tabs nav-white">
                        <!-- start tab link -->

                        <li class="active"><a href="#dashboard" data-toggle="tab">Dashboard</a></li>
                        <?php
                          if (!empty($associations['hasOne'])) :
                            foreach ($associations['hasOne'] as $alias => $details): 
                              $myTag = str_replace(' ', '', Inflector::humanize($details['controller']) );
                            ?>

                            <li><a href="#<?php echo $myTag ;?>" data-toggle="tab"><?php echo Inflector::humanize($details['controller']) ;?></a></li>
                        <?php
                            endforeach;
                          endif; ?>
                          <!-- end of hasOne -->
                          <!-- start hasMany -->
                          <?php
                          if (empty($associations['hasMany'])) {
                            $associations['hasMany'] = array();
                          }
                          if (empty($associations['hasAndBelongsToMany'])) {
                            $associations['hasAndBelongsToMany'] = array();
                          }
                          $relations = array_merge($associations['hasMany'], $associations['hasAndBelongsToMany']);
                          foreach ($relations as $alias => $details):
                            $otherSingularVar = Inflector::variable($alias);
                            $otherPluralHumanName = Inflector::humanize($details['controller']);
                            $myTag = str_replace(' ', '', $otherPluralHumanName);
                            ?>
                            <li><a href="#<?php echo $myTag ;?>" data-toggle="tab"><?php echo $otherPluralHumanName ;?></a></li>
                          <?php endforeach; ?>  
                          <!-- end hasMany -->
                        <!-- end tab link -->
                        </ul>
                      </div>
                    </header>
                    <!-- end tab content headers -->
                    <!-- tab content article-->
                    <article class="scrollable">
                      <!-- tab content div -->
                      <div class="tab-content">
                        <!-- start tab content -->
                        <div class="tab-pane padder active" id="dashboard">
                          <section class="scrollable wrapper">
                            <div class="row">
                              <div class="col-sm-6 portlet">
                                <!-- <div class="portlet"> -->
                                <section class="panel panel-info portlet-item">
                                  <header class="panel-heading">
                                    Training Completion
                                  </header>

                                  <div class="panel-body text-center">
                                    <h4>62.5<small> hrs</small></h4>
                                    <small class="text-muted block">Updated at 2 minutes ago</small>
                                    <div class="inline">
                                      <div class="easypiechart easyPieChart" data-percent="75" data-line-width="16" data-loop="false" data-size="188" style="width: 188px; height: 188px; line-height: 188px;">
                                        <span class="h2 step">75</span>%
                                        <div class="easypie-text">New</div>
                                      <canvas width="206" height="206" style="width: 188px; height: 188px;"></canvas></div>
                                    </div>                      
                                  </div>
                                  <div class="panel-footer"><small>% of avarage rate of the Conversion</small></div>
                                </section>
                              </div>
                              <div class="col-sm-6 portlet">
                                <section class="panel panel-info portlet-item">
                                  <header class="panel-heading bg-primary dker no-border"><strong>Calendar</strong></header>
                                  <div id="calendar" class="bg-primary m-l-n-xxs m-r-n-xxs"></div>
                                  <div class="list-group">
                                    <a href="#" class="list-group-item text-ellipsis">
                                      <span class="badge bg-danger">7:30</span> 
                                      Meet a friend
                                    </a>
                                    <a href="#" class="list-group-item text-ellipsis"> 
                                      <span class="badge bg-success">9:30</span> 
                                      Have a kick off meeting with .inc company
                                    </a>
                                    <a href="#" class="list-group-item text-ellipsis">
                                      <span class="badge bg-light">19:30</span>
                                      Milestone release
                                    </a>
                                  </div>
                                </section>
                              </div>
                              <div class="col-sm-6 portlet">
                                <section class="panel panel-info portlet-item">
                                  <header class="panel-heading">
                                    Scores & Performance Ratings
                                  </header>

                                  <div class="panel-body text-center">
                                    <h4>62.5<small> hrs</small></h4>
                                    <small class="text-muted block">Updated at 2 minutes ago</small>
                                    <div class="inline">
                                      <div class="easypiechart easyPieChart" data-percent="75" data-line-width="16" data-loop="false" data-size="188" style="width: 188px; height: 188px; line-height: 188px;">
                                        <span class="h2 step">75</span>%
                                        <div class="easypie-text">New</div>
                                      <canvas width="206" height="206" style="width: 188px; height: 188px;"></canvas></div>
                                    </div>                      
                                  </div>
                                  <div class="panel-footer"><small>% of avarage rate of the Conversion</small></div>
                                </section>
                              </div>
                              <div class="col-sm-6 portlet">
                                <section class="panel panel-default portlet-item">
                                  <header class="panel-heading">
                                    <ul class="nav nav-pills pull-right">
                                      <li>
                                        <a href="#" class="panel-toggle text-muted"><i class="fa fa-caret-down text-active"></i><i class="fa fa-caret-up text"></i></a>
                                      </li>
                                    </ul>
                                    News <span class="badge bg-info">32</span>                    
                                  </header>
                                  <section class="panel-body">
                                    <article class="media">
                                      <div class="pull-left">
                                        <span class="fa-stack fa-lg">
                                          <i class="fa fa-circle fa-stack-2x"></i>
                                          <i class="fa fa-bold fa-stack-1x text-white"></i>
                                        </span>
                                      </div>
                                      <div class="media-body">                        
                                        <a href="#" class="h4">Bootstrap 3: What you need to know</a>
                                        <small class="block m-t-xs">Sleek, intuitive, and powerful mobile-first front-end framework for faster and easier web development.</small>
                                        <em class="text-xs">Posted on <span class="text-danger">Feb 23, 2013</span></em>
                                      </div>
                                    </article>
                                    <div class="line pull-in"></div>
                                    <article class="media">
                                      <div class="pull-left">
                                        <span class="fa-stack fa-lg">
                                          <i class="fa fa-circle fa-stack-2x text-info"></i>
                                          <i class="fa fa-file-o fa-stack-1x text-white"></i>
                                        </span>
                                      </div>
                                      <div class="media-body">
                                        <a href="#" class="h4">Bootstrap documents</a>
                                        <small class="block m-t-xs">There are a few easy ways to quickly get started with Bootstrap, each one appealing to a different skill level and use case. Read through to see what suits your particular needs.</small>
                                        <em class="text-xs">Posted on <span class="text-danger">Feb 12, 2013</span></em>
                                      </div>
                                    </article>
                                    <div class="line pull-in"></div>
                                    <article class="media">
                                      <div class="pull-left">
                                        <span class="fa-stack fa-lg">
                                          <i class="fa fa-circle fa-stack-2x icon-muted"></i>
                                          <i class="fa fa-mobile fa-stack-1x text-white"></i>
                                        </span>
                                      </div>
                                      <div class="media-body">
                                        <a href="#" class="h4 text-success">Mobile first html/css framework</a>
                                        <small class="block m-t-xs">Bootstrap, Ratchet</small>
                                        <em class="text-xs">Posted on <span class="text-danger">Feb 05, 2013</span></em>
                                      </div>
                                    </article>
                                  </section>
                                </section>
                              </div>
                              <div class="col-sm-6 portlet">
                                <section class="panel panel-success portlet-item">
                                  <header class="panel-heading">
                                    Connection
                                  </header>
                                  <ul class="list-group alt">
                                    <li class="list-group-item">
                                      <div class="media">
                                        <span class="pull-left thumb-sm"><img src="images/avatar.jpg" alt="John said" class="img-circle"></span>
                                        <div class="pull-right text-success m-t-sm">
                                          <i class="fa fa-circle"></i>
                                        </div>
                                        <div class="media-body">
                                          <div><a href="#">Chris Fox</a></div>
                                          <small class="text-muted">about 2 minutes ago</small>
                                        </div>
                                      </div>
                                    </li>
                                    <li class="list-group-item">
                                      <div class="media">
                                        <span class="pull-left thumb-sm"><img src="images/avatar.jpg" alt="John said" class="img-circle"></span>
                                        <div class="pull-right text-muted m-t-sm">
                                          <i class="fa fa-circle"></i>
                                        </div>
                                        <div class="media-body">
                                          <div><a href="#">Amanda Conlan</a></div>
                                          <small class="text-muted">about 2 hours ago</small>
                                        </div>
                                      </div>
                                    </li>
                                    <li class="list-group-item">
                                      <div class="media">
                                        <span class="pull-left thumb-sm"><img src="images/avatar.jpg" alt="John said" class="img-circle"></span>
                                        <div class="pull-right text-warning m-t-sm">
                                          <i class="fa fa-circle"></i>
                                        </div>
                                        <div class="media-body">
                                          <div><a href="#">Dan Doorack</a></div>
                                          <small class="text-muted">3 days ago</small>
                                        </div>
                                      </div>
                                    </li>
                                    <li class="list-group-item">
                                      <div class="media">
                                        <span class="pull-left thumb-sm"><img src="images/avatar.jpg" alt="John said" class="img-circle"></span>
                                        <div class="pull-right text-danger m-t-sm">
                                          <i class="fa fa-circle"></i>
                                        </div>
                                        <div class="media-body">
                                          <div><a href="#">Lauren Taylor</a></div>
                                          <small class="text-muted">about 2 minutes ago</small>
                                        </div>
                                      </div>
                                    </li>
                                  </ul>
                                </section>
                              </div>
                            </div>
                          </section>
                        </div>
                        <!-- start hasOne -->
                        <?php
                          if (!empty($associations['hasOne'])) :
                            foreach ($associations['hasOne'] as $alias => $details): 
                              $myTag = str_replace(' ', '', Inflector::humanize($details['controller']) );
                            ?>

                            <div class="tab-pane padder" id="<?php echo $myTag ;?>">
                              <h3><?php echo "<?php echo __('Related " . Inflector::humanize($details['controller']) . "'); ?>"; ?></h3>
                            <?php echo "<?php if (!empty(\${$singularVar}['{$alias}'])): ?>\n"; ?>
                              <dl>
                            <?php
                                foreach ($details['fields'] as $field) {
                                  if (!in_array($field, array('created', 'modified', 'id'))) {
                                    echo "\t\t<dt><?php echo __('" . Inflector::humanize($field) . "'); ?></dt>\n";
                                    echo "\t\t<dd>\n\t<?php echo \${$singularVar}['{$alias}']['{$field}']; ?>\n&nbsp;</dd>\n";
                                  }
                                }
                            ?>
                              </dl>
                            <?php echo "<?php endif; ?>\n"; ?>
                              <div class="actions">
                                <!-- <ul>
                                  <li><?php echo "<?php echo \$this->Html->link(__('Edit " . Inflector::humanize(Inflector::underscore($alias)) . "'), array('controller' => '{$details['controller']}', 'action' => 'edit', \${$singularVar}['{$alias}']['{$details['primaryKey']}'])); ?></li>\n"; ?>
                                </ul> -->
                                <div class="col-sm-5 m-b-xs">
                                    <?php 
                                      echo "\t\t\t<?php echo \$this->Html->link(' Edit', array('controller' => '{$details['controller']}', 'action' => 'edit', \${$singularVar}['{$alias}']['{$details['primaryKey']}']), array('class' => 'btn btn-default bg-success fa fa-pencil width:25px; height:25px; padding-top:5px', 'data-toggle'=>'ajaxModal', 'escape' => false)); ?>\n";
                                    ?> 
                                </div>
                              </div>
                            </div>
                        <?php
                            endforeach;
                          endif; ?>
                          <!-- end hasOne -->
                          <!-- start hasMany -->
                        <?php
                          if (empty($associations['hasMany'])) {
                            $associations['hasMany'] = array();
                          }
                          if (empty($associations['hasAndBelongsToMany'])) {
                            $associations['hasAndBelongsToMany'] = array();
                          }
                          $relations = array_merge($associations['hasMany'], $associations['hasAndBelongsToMany']);

                          foreach ($relations as $alias => $details) {
                            $otherSingularVar = Inflector::variable($alias);
                            $otherPluralHumanName = Inflector::humanize($details['controller']);
                            $myTagMany = str_replace(' ', '', $otherPluralHumanName);
                            ?>
                          <div class="tab-pane padder" id="<?php echo $myTagMany ;?>">
                            <h3><?php echo "<?php echo __('Related " . $otherPluralHumanName . "'); ?>"; ?></h3>
                            

                            <?php echo "<?php if (!empty(\${$singularVar}['{$alias}'])): ?>\n"; ?>
                            <section class="panel panel-default padder">
                              <div class="row wrapper">
                                <div class="col-sm-5 m-b-xs">
                                    <?php 
                                    echo "\t\t\t<?php echo \$this->Html->link(' Create', array('controller' => '{$details['controller']}', 'action' => 'add', '?' => array('returnURL' => \$this->Html->url( null, true ))), array('class' => 'btn btn-default bg-success fa fa-plus', 'data-toggle'=>'ajaxModal', 'style'=>'color:#000;', 'escape' => false)); ?>\n";
                                  ?> 
                                </div>
                              </div>
                              <div class="table-responsive">
                                <table class="table table-striped b-t b-light">
                                  <thead>
                                    <tr>
                                      <th class="actions"><?php echo "<?php echo __('Actions'); ?>"; ?></th>
                                      <?php foreach ($details['fields'] as $field) : ?>
                                      <?php if (!in_array($field, array('created', 'modified','id'))) : ?>
                                      <th>
                                        <?php echo "\t\t<?php echo __('" . Inflector::humanize($field) . "'); ?>\n"; ?>
                                      </th>
                                    <?php endif; ?>
                                      <?php endforeach; ?>
                                    </tr>
                                  </thead>
                                  <tfoot>
                                    <tr>
                                      <th class="actions"><?php echo "<?php echo __('Actions'); ?>"; ?></th>
                                      <?php foreach ($details['fields'] as $field) : ?>
                                      <?php if (!in_array($field, array('created', 'modified','id'))) : ?>
                                      
                                        <?php echo "\t\t<th><?php echo __('" . Inflector::humanize($field) . "'); ?></th>\n"; ?>
                                      
                                    <?php endif; ?>
                                      <?php endforeach; ?>
                                    </tr>
                                  </tfoot>
                                  
                                  <tbody>
                                  <?php
                                    echo "\t<?php foreach (\${$singularVar}['{$alias}'] as \${$otherSingularVar}): ?>\n";
                                      echo "\t\t<tr>\n";
                                      echo "\t\t\t<td class=\"actions\">\n";

                                      echo "\t\t\t\t<?php echo \$this->Html->link('', array('controller' => '{$details['controller']}', 'action' => 'sneak', \${$otherSingularVar}['{$details['primaryKey']}']), array('class'=>'btn btn-xs btn-success fa fa-desktop', 'style'=>'color:#000;width:25px; height:25px; padding-top:5px','data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>'View Details', 'data-toggle'=>'ajaxModal', 'escape' => false)); ?>\n";
                                      
                                      echo "\t\t\t\t<?php echo \$this->Html->link('', array('controller' => '{$details['controller']}', 'action' => 'edit', \${$otherSingularVar}['{$details['primaryKey']}']), array('class'=>'btn btn-xs btn-warning fa fa-pencil', 'style'=>'color:#000;width:25px; height:25px; padding-top:5px','data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>'Modify Details', 'data-toggle'=>'ajaxModal', 'escape' => false)); ?>\n";
                                      echo "\t\t\t</td>\n";
                                      // \$this->Form->button('<i class=\"fa fa-pencil\"></i>', array('class'=>'btn btn-xs btn-warning', 'style'=>'color:#000;width:25px; height:25px;','data-toggle'=>'tooltip', 'data-placement'=>'right', 'title'=>'Modify Details'))

                                      foreach ($details['fields'] as $field) {
                                        if (!in_array($field, array('created', 'modified','id'))) { 
                                          echo "\t\t\t<td><?php echo \${$otherSingularVar}['{$field}']; ?></td>\n";  
                                        }
                                      }
                                      echo "\t\t</tr>\n";
                                    echo "\t<?php endforeach; ?>\n";
                                  ?>
                                    <tr>
                                    </tr>
                                  </tbody>
                                </table>
                              </div>
                            <?php echo "<?php endif; ?>\n\n"; ?>
                            <br/>
                            <div class="actions">
                              <div class="col-sm-5 m-b-xs">
                                  <?php 
                                    echo "\t\t\t<?php echo \$this->Html->link(' Create', array('controller' => '{$details['controller']}', 'action' => 'add', '?' => array('returnURL' => \$this->Html->url( null, true ))), array('class' => 'btn btn-default bg-success fa fa-plus', 'data-toggle'=>'ajaxModal', 'style'=>'color:#000;', 'escape' => false)); ?>\n";
                                  ?> 
                              </div>
                            </div>
                          </div>
                          <?php } ?>
                          <!-- end hasMany -->
                        <!-- end tab content -->
                      </div><!-- end tab content div -->
                    </article> <!-- end tab content article-->
                  </section>
                </aside>
                <?php 
                  endif; ?>
                <!-- end column 2 -->
              </section>
            </section>
          </section>
          <!-- end content view page -->

